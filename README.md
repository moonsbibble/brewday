# BrewDay

## Description
BrewDay is a web application written in Java for personal beer management. In this software, equipment, ingredients, and beer recipes can be added and removed to manage a personal brewmaster. The main page is entirely devoted to automatic calculation of beers that can be brewed based on available equipment and ingredients, facilitating the user experience.

## Architecture
The application consists of 3 levels: 
- **Level 1** — Presentation level written with JSP pages and CSS style sheets
- **Level 2** — Java business logic layer using the Spring framework
- **Level 3** — Persistence level of data via database using MySQL

(It features Apache Tomcat Server 9 to run the local the website).

## Project Stages
The project for the Software Engineering course started with the definition of use cases, sequence diagrams, architectures and UML. Next, implementation logics were chosen including the technologies and design patterns to be used. Finally, a software analysis phase allowed us to detect code smells and antipatterns using SonarQube and Understand tools.

## Overview
![Download](images/video.mov){width=100%}
